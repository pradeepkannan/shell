$(document).ready(function(){ 
    $(window).on('resize',function(){dashboardEcharts();});$(window).on('load',function(){dashboardEcharts();});$("a[data-toggle=\"tab\"]").on("shown.bs.tab",function(e){dashboardEcharts();});function dashboardEcharts(){var myChart=echarts.init(document.getElementById('main'));var option={tooltip:{trigger:'item',formatter:function(params){var date=new Date(params.value[0]);var data=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes();return data+'<br/>'+params.value[1]+', '+params.value[2];},responsive:true},dataZoom:{show:true,start:70},legend:{data:['Profit']},grid:{y2:80},xAxis:[{type:'time',splitNumber:10}],yAxis:[{type:'value'}],series:[{name:'Profit',type:'line',showAllSymbol:true,symbolSize:function(value){return Math.round(value[2]/10)+2;},data:(function(){var d=[];var len=0;var now=new Date();var value;while(len++<200){d.push([new Date(2014,9,1,0,len*10000),(Math.random()*30).toFixed(2)-0,(Math.random()*100).toFixed(2)-0]);}
return d;})()}]};myChart.setOption(option);}
$(".theme-loader").animate({opacity:"0"},1000);
setTimeout(function(){$(".theme-loader").remove();},1000);
$('#simpletable').DataTable({"paging":true,"ordering":true,"bLengthChange":true,"info":true,"searching":true});
$('#edit-cancel').on('click',function(){
    var c=$('#edit-btn').find("i");
    c.removeClass('icofont-close');
    c.addClass('icofont-edit');$('.view-info').show();
    $('.edit-info').hide();
}) ;
$('.edit-info').hide();
    $('#edit-btn').on('click',function(){
        var b=$(this).find("i");
        var edit_class=b.attr('class');
        if(edit_class=='icofont icofont-edit'){b.removeClass('icofont-edit');b.addClass('icofont-close');$('.view-info').hide();$('.edit-info').show();}else{b.removeClass('icofont-close');b.addClass('icofont-edit');$('.view-info').show();$('.edit-info').hide();}});CKEDITOR.replace('description',{toolbar:[{name:'clipboard',items:['Undo','Redo']},{name:'styles',items:['Styles','Format']},{name:'basicstyles',items:['Bold','Italic','Strike','-','RemoveFormat']},{name:'paragraph',items:['NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote']},{name:'links',items:['Link','Unlink']},{name:'insert',items:['Image','EmbedSemantic','Table']},{name:'tools',items:['Maximize']},{name:'editing',items:['Scayt']}],customConfig:'',extraPlugins:'autoembed,embedsemantic,image2,uploadimage,uploadfile',imageUploadUrl:'/uploader/upload.php?type=Images',uploadUrl:'/uploader/upload.php',removePlugins:'image',height:400,bodyClass:'article-editor',format_tags:'p;h1;h2;h3;pre',removeDialogTabs:'image:advanced;link:advanced',stylesSet:[{name:'Marker',element:'span',attributes:{'class':'marker'}},{name:'Cited Work',element:'cite'},{name:'Inline Quotation',element:'q'},{name:'Special Container',element:'div',styles:{padding:'5px 10px',background:'#eee',border:'1px solid #ccc'}},{name:'Compact table',element:'table',attributes:{cellpadding:'5',cellspacing:'0',border:'1',bordercolor:'#ccc'},styles:{'border-collapse':'collapse'}},{name:'Borderless Table',element:'table',styles:{'border-style':'hidden','background-color':'#E6E6FA'}},{name:'Square Bulleted List',element:'ul',styles:{'list-style-type':'square'}},{name:'240p',type:'widget',widget:'embedSemantic',attributes:{'class':'embed-240p'}},{name:'360p',type:'widget',widget:'embedSemantic',attributes:{'class':'embed-360p'}},{name:'480p',type:'widget',widget:'embedSemantic',attributes:{'class':'embed-480p'}},{name:'720p',type:'widget',widget:'embedSemantic',attributes:{'class':'embed-720p'}},{name:'1080p',type:'widget',widget:'embedSemantic',attributes:{'class':'embed-1080p'}}]});$('#edit-cancel-btn').on('click',function(){var c=$('#edit-info-btn').find("i");c.removeClass('icofont-close');c.addClass('icofont-edit');$('.view-desc').show();$('.edit-desc').hide();});$('.edit-desc').hide();$('#edit-info-btn').on('click',function(){var b=$(this).find("i");var edit_class=b.attr('class');if(edit_class=='icofont icofont-edit'){b.removeClass('icofont-edit');b.addClass('icofont-close');$('.view-desc').hide();$('.edit-desc').show();}else{b.removeClass('icofont-close');b.addClass('icofont-edit');$('.view-desc').show();$('.edit-desc').hide();}});$('.demo').each(function(){$(this).minicolors({control:$(this).attr('data-control')||'hue',defaultValue:$(this).attr('data-defaultValue')||'',format:$(this).attr('data-format')||'hex',keywords:$(this).attr('data-keywords')||'',inline:$(this).attr('data-inline')==='true',letterCase:$(this).attr('data-letterCase')||'lowercase',opacity:$(this).attr('data-opacity'),position:$(this).attr('data-position')||'bottom left',swatches:$(this).attr('data-swatches')?$(this).attr('data-swatches').split('|'):[],change:function(value,opacity){if(!value)return;if(opacity)value+=', '+opacity;if(typeof console==='object'){console.log(value);}},theme:'bootstrap'});});
var url="https://ask4deli.in/oms/backend/";

});
/*function sel()
{
   // alert("hi");
    $.ajax({

        type:"POST",
        url:url+"view_user.php",
        crossDomain:true,
        cache: false,
     
        success: function(response)
        {
            console.log(response);
           // var field=JSON.parse(data);

             var jsondata = JSON.parse(response);
            var co = "";
            if(jsondata[0].status == "success"){
                for(var i=0;i<jsondata.length;i++)
                {
                    var date = jsondata[i].id;
                    var amount=jsondata[i].username;
                    var action_type=jsondata[i].statu;
                   // var username = jsondata[i].username;
                    /*
                    var sno = i+1;
                    var user_id=jsondata[i].user_id;
                    var user_name=jsondata[i].user_name;
                    var email_id=jsondata[i].email_id;
                    var amount=jsondata[i].amount;
                    
                   co+='<tr><td class="sorting_1"> '+date+'</td><td ><a href="http://html.phoenixcoded.net/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="78191a1b494a4b381f15191114561b1715">'+amount+'</a></td><td >'+action_type+'</td><td><label class="switch"><input type="checkbox"><span class="slider round"></span></label></td><td class="dropdown"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i></button><div class="dropdown-menu dropdown-menu-right b-none contact-menu"><a class="dropdown-item" href="#!"><i class="icofont icofont-edit"></i>Edit</a><a class="dropdown-item" href="#!"><i class="icofont icofont-ui-delete"></i>Delete</a></div></td></tr>';
                
                
                
                }

}
/*
            for(i=0;i<field.length;i++)
            {
                var id=field[i].id;
                console.log(id);
                var username=field[i].username;
                console.log(username);
                var status=field[i].status;

               // var email=field[i].email;
               // var password=field[i].password;
               // var confirmpassword=field[i].confirmpassword;
                
                
             
                     
               // $("#simpletable").append('<tr><td>'+id+'</td><td>'+username+'</td><td>'+email+'</td><td>'+password+'</td><td>'+confirmpassword+'</td><td><input type="checkbox" data-usid="'+id+'"  onchange="userChange(this);" checked data-toggle="toggle"></td><td><button class="edit" data-nid="'+id+'" onclick="editUser(this);" >Edit</button><button class="update" onClick="updatedata(this);" id="'+id+'" >Update</button><button class="btn btn-danger btn-md deleteRecord" onClick="deletedata(this);" id="'+id+'">Delete</button></td></tr>');
               $("#simpletable tbody").append('<tr><td>'+id+'</td><td>'+username+'</td><td>'+status+'</td></tr>');
            }
        }
       $('.table tbody').append(co);
    }
});
}*/